#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.query;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.osgi.extensions.annotation.ServiceReference;
import org.squashtest.tm.api.report.criteria.Criteria;
import org.squashtest.tm.api.report.query.ReportQuery;
import org.squashtest.tm.api.repository.SqlQueryRunner;

public class Query implements ReportQuery{

	private Resource sql;
	
	private SqlQueryRunner runner;

    @ServiceReference
    public void setRunner(SqlQueryRunner runner) {
            this.runner = runner;
    }
	

	/**
	 * This method should run the method query (or queries) and populate a Map containing the dataset(s) mapped by the
	 * name expected by the report.
	 * 
	 * @param criteria
	 *            the criteria to apply to the request.
	 * @param model
	 *            a map which should be populated with the dataset(s)
	 * @return
	 */
	@Override
	public void executeQuery(Map<String, Criteria> criteria, Map<String, Object> model) {
		
		
		//set whatever you like in the model
		//if you don't know why the key is "data", just leave it as is.
		model.put("data", new ArrayList(0));
	}
	
	
	public void setSql(Resource sql){
		this.sql = sql;
	}
	
	
}
